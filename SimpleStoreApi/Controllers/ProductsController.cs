﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleStoreApi.Models;
using SimpleStoreApi.Models.Data;
using SimpleStoreApi.Models.DAO;

namespace SimpleStoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductDAO _productDAO;

        public ProductsController(IProductDAO productDAO)
        {
            _productDAO = productDAO;
        }


        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            try
            {
                var listProducts = await _productDAO.GetAllProducts();
                
                if(listProducts == null)
                {
                    return NotFound();
                }

                return Ok(listProducts);
                   
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(Guid id)
        {
            try
            {
                var product = await _productDAO.GetProductById(id);

                if (product == null)
                {
                    return NotFound();
                }

                return Ok(product);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(Product product)
        {
           
            try
            {
                var productChanged = await  _productDAO.ChangeProduct(product);

                if(productChanged == null)
                {
                    return BadRequest();
                }

                 return Ok(product);
            }
            catch (DbUpdateConcurrencyException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: api/Products
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(Product product)
        {
            try
            {
                var productCreated = await _productDAO.ChangeProduct(product);

                if(productCreated == null)
                {
                    return BadRequest();
                }
                
                return Ok(productCreated);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Product>> DeleteProduct(Guid id)
        {
            try
            {
               var productRemoved = await _productDAO.RemoveProduct(id);

               if(productRemoved == false)
               {
                   return NotFound();
               }

               return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

    }
}
