﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimpleStoreApi.Models;
using Microsoft.EntityFrameworkCore;

namespace SimpleStoreApi.Models.Data
{
    public class SimpleStoreContext : DbContext
    {
        public SimpleStoreContext(DbContextOptions<SimpleStoreContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(p => new { p.ID });
            modelBuilder.Entity<Product>().HasOne(c => c.Category).WithMany(p => p.Products);

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }

        }
    }
