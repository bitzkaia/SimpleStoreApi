﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleStoreApi.Models.Data;

namespace SimpleStoreApi.Models.DAO
{
    public interface IProductDAO
    {
        Task<List<Product>> GetAllProducts();
        Task<Product> GetProductById(Guid id);
        Task<Product> ChangeProduct(Product product);
        Task<Product> CreateProduct(Product product);
        Task<bool> RemoveProduct(Guid id);
    }

    public class ProductDAO : IProductDAO
    {
        private readonly SimpleStoreContext _context;

        public ProductDAO(SimpleStoreContext context)
        {
            _context = context;
        }

        public async Task<List<Product>> GetAllProducts()
        {
            List<Product> x = await _context.Products.ToListAsync();

            return x;
        }

        public async Task<Product> GetProductById(Guid id)
        {
            Product x = await _context.Products.Where(p => p.ID == id).FirstOrDefaultAsync();

            return x;
        }

        public async Task<Product> ChangeProduct(Product product)
        {
            var baseProduct = await _context.Products.Where(p => p.ID.Equals(product.ID)).FirstOrDefaultAsync();

            if (baseProduct != null)
            { 
                baseProduct.Name = product.Name;
                baseProduct.Price = product.Price;
                baseProduct.Quantity = product.Quantity;
                baseProduct.Description = product.Description;
                baseProduct.Category = product.Category;
                int saveResult = await _context.SaveChangesAsync();
            }   

            return baseProduct;
        }

        public async Task<Product> CreateProduct(Product product)
        {
            if(product != null)
            {
                _context.Products.Add(product);
                var saveResult = await _context.SaveChangesAsync();
            }

            return product;
        }

        public async Task<bool> RemoveProduct(Guid id)
        {
            var product = _context.Products.Where(x => x.ID.Equals(id)).FirstOrDefaultAsync();

            if(product != null)
            {
                _context.Remove(product);
                var saveResult = await _context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false; 
            }
            
        }

    }
}
