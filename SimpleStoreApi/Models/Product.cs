﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleStoreApi.Models
{
    public class Product
    {
        public Guid ID {get; set;}
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }
        public Category Category { get; set; }
    }
}
